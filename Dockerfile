FROM node:16.20.2-alpine as build

WORKDIR /app
RUN apk update && apk add git && \
    git clone https://gitfront.io/r/deusops/JnacRhR4iD8q/2048-game.git


WORKDIR /app/2048-game
RUN npm install --only=dev && npm run build



FROM nginx:alpine

RUN rm -rf /usr/share/nginx/html/*

COPY --from=build /app/2048-game/dist  /usr/share/nginx/html

ENTRYPOINT ["nginx","-g","daemon off;"]


