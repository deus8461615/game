variable "image_flavor" {
  type = string
  default = "Ubuntu-22.04-202208"
}

variable "compute_flavor" {
  type = string
  default = "Basic-1-2-20"
}

variable "key_pair_name" {
  type = string
  default = "terraform-key1"
}

variable "availability_zone_name" {
  type = string
  default = "MS1"
}

variable "vk_us" {
  type = string
  sensitive = true
}

variable "VK_PASS" {
  type = string
  sensitive = true
}

variable "project_id" {
  type = string
  sensitive = true
}
