data "vkcs_networking_network" "extnet" {
  name = "ext-net"
}

resource "vkcs_networking_network" "common-public" {
  name = "common-public-net"
}

resource "vkcs_networking_subnet" "common-public" {
  name            = "common-public-subnet"
  network_id      = vkcs_networking_network.common-public.id
  cidr            = "192.168.200.0/24"
  dns_nameservers = ["8.8.8.8", "8.8.4.4"]
}

resource "vkcs_networking_router" "common-public" {
  name                = "common-public-router"
  admin_state_up      = true
  external_network_id = data.vkcs_networking_network.extnet.id
}

resource "vkcs_networking_router_interface" "common-public" {
  router_id = vkcs_networking_router.common-public.id
  subnet_id = vkcs_networking_subnet.common-public.id
}
