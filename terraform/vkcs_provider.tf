terraform {
    required_providers {
        vkcs = {
            source = "vk-cs/vkcs"
            version = "~> 0.5.2" 
        }
    }
}

provider "vkcs" {
    username = var.vk_us
    password = var.VK_PASS
    project_id = var.project_id
    region = "RegionOne"
    auth_url = "https://infra.mail.ru:35357/v3/" 
}