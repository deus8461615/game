resource "vkcs_networking_network" "common-private" {
  name = "common-private-net"
}

resource "vkcs_networking_subnet" "common-private" {
  name            = "common-private-subnet"
  network_id      = vkcs_networking_network.common-private.id
  cidr            = "192.168.199.0/24"
  dns_nameservers = ["8.8.8.8", "8.8.4.4"]
}

resource "vkcs_networking_router" "common-private" {
  name           = "common-private-router"
  admin_state_up = true
}

resource "vkcs_networking_router_interface" "common-private" {
  router_id = vkcs_networking_router.common-private.id
  subnet_id = vkcs_networking_subnet.common-private.id
}
